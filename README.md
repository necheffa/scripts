# scripts

A dumping ground for various utility scripts not significant enough to merit their own repo.

## License Information

In general, all scripts in this repo are licensed under the terms of the GPLv3 unless otherwise specified in
the individual script file(s). A copy of the GPLv3 is provided in the root of this repo as `COPYING`.

## Script Details

A brief description of the scripts in this repo.

### backup\_lvm

`backup_lvm` is a bash shell script that depends on ext2/3/4 dump, LVM, gzip, and a few other standard shell utilities to
backup logical volumes specified in the `/etc/fstab` file. It currently makes a lot of assumptions about what your system
looks like and does not provide nearly enough run-time configuration to override this.
It has a few TODOs in the script itself that need addressed eventually.
It is intended to be run as a cronjob but can be run directly too.

### backup\_btrfs

`backup_btrfs` is a bash shell script modeled after `backup_lvm`, but for btrfs file systems. Native btrfs snapshots are used
which are then tarballed with gzip compression. This was done, as opposed to native send/receive archives, to make extraction of individual
files easier and integrate with other tooling. However, a future version may favor native send/receive archives.

### songconv

`songconv` is a bash shell script that depends on GNU parallel and ffmpeg to bulk convert audio files in parallel.
Currently songconv only converts FLAC to OGG because I rip my CD's to FLAC for archival then encode to OGG for
mobile playback. Eventually, I'd like to add more flexability, possibly even do an entire rewrite in Python3.

### passgen

`passgen` uses /dev/urandom to generate 20 character base85 passwords.

### photocleaner

`photocleaner` is a Perl script that uses exif raitings to automatically delete photos. Originally designed to work with Darktable,
a raiting of `-1` will set a file to be deleted, in Darktable this is accomplished by choosing the red X instead of a star-based raiting.

### remote\_backup

`remote_backup` is a bash script that remotely executes `backup_lvm` and rotates old backups. It is intended to be run from a
cron job but may be called on the command line directly. It requires an `rbackup` user to exist on both the local and remote machine.
`sudo` should be configured on the remote machine to permit the `rbackup` user to execute `backup_lvm` and `clear_backups` with elevated privilege.
Password-less SSH public key authentication should be setup to permit the remote execution of commands between the local and remote system.

To setup remote\_backup on Debian:

* Create an rbackup user on the remote and local systems: `adduser --disabled-password --group` the `--disabled-password` option prevents local logins but does allow for SSH logins.
* Add the rbackup user to the `backup` group, one of the Debian specified groups: `gpasswd -a rbackup backup`
* Generate a SSH key-pair without a password: `ssh-keygen -t ed25519 -f rbackup` and install the public key on each remote host.
* On remote systems, make sure `/data/bkup` has the appropriate permissions and ownership: `chown root:backup /data/bkup && chmod 0750 /data/bkup`
* On remote systems, edit the sudo configuration so that rbackup is able to execute backup\_lvm and clear\_backups by editing `/etc/sudoers.d/rbackup` and
adding the following lines: `rbackup $HOSTNAME = (root) NOPASSWD: /usr/local/sbin/backup_lvm` and `rbackup $HOSTNAME = (root) NOPASSWD: /usr/local/sbin/clear_backups`.
Be sure to replace `$HOSTNAME` with the acutal hostname of the remote machine.
* On the local machine, make sure `/var/local/backups` has the appropriate permissions and ownership: `chown root:backup /var/local/backups && chmod 2770 /var/local/backups`
  We recommend that `/var/local/backups` exists on its own filesystem to avoid problems or excessively large backups from clogging up the root filesystem.

### clear\_backups

`clear_backups` is a bash shell script used as part of `remote_backup` so that the `rbackup` user can be given limited privilege to manage backup disk space.

### psfw

`psfw` is a bash shell script that provides a NetworkManager dispatcher facilitating profile specific firewalls. /usr/local/etc/psfw.conf is read to
try and match the current network UUID to a list of UUIDs and then set a named firewall profile. Within psfw.conf, each line is a record entry and each
entry is formated as UUID:PROFILE. Each PROFILE is the name of a firewall script stored under /usr/local/etc/psfw/. If no match can be found in psfw.conf
then `psfw` attempts to set the /usr/local/etc/psfw/default firewall script which it assumes to be the most restrictive firewall script installed.
Each firewall script is just a shell script containing groups of iptables commands to be executed. We trust only root has write access to /usr/local/etc/.

### base85

`base85` is a Python 3 script that implements the IETF RFC 1924 base85 binary to ASCII conversion. It reads input from STDIN and writes to STDOUT and
is intended to be used in Unix command pipelines.

### sysbench

`sysbench` is a bash shell script that collects CPU and GPU data points and writes them to STDOUT. It is intended to just be redirected to a text file
while an application is run so that the user can see how that workload uses the system. A lot of processes get spawned by the script but for heavy workloads
that doesn't really skew the results much at all. Currently only Nvidia GPUs are supported.

