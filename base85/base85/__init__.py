"""
Implements base85 as describe in IETF RFC 1924 as a Unix pipeline command.
Reference: https://tools.ietf.org/html/rfc1924
"""

#    Copyright (C) 2020, 2023 Alexander Necheff
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>

from sys import stdin
from decimal import getcontext
from argparse import ArgumentParser

from .base85 import blockToBase85
from .base85 import blockToDecimal
from .base85 import pageToBase85Str

__version__ = "1.1.0"
__cpyear__ = "2020, 2023"

def main():
    parser = ArgumentParser(
            prog="base85",
            description="Implements base85 as describe in IETF RFC 1924 as a Unix pipeline command.")

    parser.add_argument("-v", "--version", action="store_true", default=False)

    args = parser.parse_args()

    if args.version:
        msg = "base85 v"+__version__+"\n"
        msg += "Copyright (C) "+__cpyear__+" Alexander Necheff\n"
        msg += "This program is released under the terms of the GPLv3."
        print(msg)
        return 0

    # 128 is probably too many digits to show, I should figure out what the biggest 128 bit value is and how many digits it would take to write in base10,
    # then that is value to set my precision to.
    getcontext().prec = 128

    page = []
    block = []
    # use stdin.buffer instead of stdin so we read in binary mode rather than text mode!
    byte = stdin.buffer.read(1)
    while byte != b'':
        if len(block) == 16:
            val = blockToDecimal(block)
            page = page + blockToBase85(val)
            block = []
        block.append(byte)
        byte = stdin.buffer.read(1)

    if len(block) != 0:
        # there is a partially filled block remaining but we've seen EOF
        padSize = 16 - len(block)
        for i in range(padSize):
            # pad with zeros to fill out the block
            block.append(b'\x00')
        val = blockToDecimal(block)
        page = page + blockToBase85(val)
        block = []

    s = pageToBase85Str(page)
    print(s)

