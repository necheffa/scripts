#    Copyright (C) 2020, 2023 Alexander Necheff
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>

from sys import byteorder
from decimal import Decimal

# the base85 collating sequence as specified by RFC 1924
collatingSequence = [  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        '!', '#', '$', '%', '&', '(', ')', '*', '+', '-', ';', '<', '+',
        '>', '?', '@', '^', '_', '`', '{', '|', '}', '~' ]

def blockToBase85(val):
    """
    blockToBase85 takes a 128bit value as a Decimal object and returns the base85 mapping as
    a list of integers suitable to index into the base85 collating sequence.
    """

    l = []

    n = val
    while n > 0:
        q = n // 85
        r = n % 85
        n = q
        l.append(int(r))

    return l

def blockToDecimal(block):
    """
    blockToDecimal takes a 16 byte vector and returns the numeric representation as a Decimal object,
    suitable to be passed into blockToBase85.
    """
    # break the block up into the most significant 8 bytes (msb), and the least significant 8 bytes (lsb)
    # The 16 byte integer is the msb left-shifted 64 bits plus the lsb
    msb = int.from_bytes(b''.join(block[0:8]), byteorder=byteorder)
    lsb = int.from_bytes(b''.join(block[8:]), byteorder=byteorder)
    summand = Decimal(msb) * 2**64 # Decimal does not have a left-shift operator apparently...
    val = summand + Decimal(lsb)

    return val

def pageToBase85Str(page):
    """
    given a page of base85 remainders, pageToBase85Str returns a string representation.
    """
    # Per RFC 1924, we work backwards through the page
    newpage = page
    newpage.reverse()
    s = str()
    for c in newpage:
        s = s + collatingSequence[c]

    return s

