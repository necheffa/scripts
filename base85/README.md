Implements base85 as describe in IETF RFC 1924 as a Unix pipeline command.
Reference: https://tools.ietf.org/html/rfc1924
