"""
Test basic functionality in the base85 package.

Reference IETF RFC 1924 for details:
    https://tools.ietf.org/html/rfc1924
"""

from decimal import Decimal
from decimal import getcontext

import pytest

from base85 import blockToBase85
from base85 import pageToBase85Str
from base85 import blockToDecimal

def test_blockToBase85():
    """
    When blockToBase85 is passed a valid block of bytes, a base85 page is returned.
    Assumes little-endian execution host.
    """
    getcontext().prec = 128
    page = blockToBase85(Decimal("21932261930451111902915077091070067066"))
    expected = "4)+k&C#VzJ4br>0wv%Yp"

    s = pageToBase85Str(page)
    assert s == expected

def test_pageToBase85Str():
    """
    When blockToDecimal is passed a valid block, a valid Decimal representation is returned.
    Assumed little-endian execution host.
    """
    # block encoding of IPv6 address 1080:0:0:0:8:800:200C:417A
    block = [ b'\x10', b'\x80', b'\x00', b'\x00',
              b'\x00', b'\x00', b'\x00', b'\x00',
              b'\x00', b'\x08', b'\x08', b'\x00',
              b'\x20', b'\x0c', b'\x41', b'\x7a' ]

    expected = "4)+k&C#VzJ4br>0wv%Yp"

    d = blockToDecimal(block)
    page = blockToBase85(d)
    s = pageToBase85Str(page)

    assert s == expected

