#!/usr/bin/env bash

# 2012-11-21 - Alexander Necheff
# passgen - generate passwords using base85 to encode
#               random data to printable characters

#    Copyright (C) 2012, 2014, 2019, 2020 Alexander Necheff
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>

# set options to make troubleshooting easier
# -e -> exit immediately if a pipeline exits with non-zero status
# -u -> treat unset variables during expansion as an error
# -o pipefail -> force intermediate non-zero values in a pipeline to be returned even if the last command in a pipline succeeds
set -eu -o pipefail

VERSION="1.0.8"
CPYEAR="2012, 2014, 2019, 2020"

# default to 20 character passwords
LEN=20

function func_eprintf {
    printf "%s\n" "$1" 1>&2
}

function func_print_usage {
    printf "%s\n" "Usage: passgen [-h] [-v] [-l LEN]"
    printf "%s\n" "Generate passwords using base85 to encode random data to prinable characters."
    printf "\n"
    printf "  %s\t\t%s\n" "-l, --length" "Optionally specify the password length. If unspecified, defaults to 20."
    printf "  %s\t\t%s\n" "-v, --version" "Print the version string and exit successfully."
    printf "  %s\t\t%s\n" "-h, --help" "Print this usage message and exit successfully."
    printf "\n"
    printf "%s\n" "LEN is assumed to be a base10 number, if characters beyond [0-9] appear then passgen will"
    printf "%s\n" "exit on error. If the -l option is unspecified, passgen defaults to a 20 character password."
}

while [ $# -gt 0 ]; do
    case "$1" in
        -l | --length)
            LEN="$2"
            shift 2
            ;;
        -h | --help)
            func_print_usage
            exit 0
            ;;
        -v | --version)
            echo "passgen v$VERSION"
            echo "Copyright (C) $CPYEAR Alexander Necheff"
            echo "This program is licensed under the terms of the GPLv3."
            exit 0
            ;;
        *)
            func_print_usage
            func_eprintf ""
            func_eprintf "passgen: Error: unrecognized argument [$1]"
            exit 1
            ;;
    esac
done

# if the user entered $LEN with non-numeric characters they will remain after tr strips all digits
NLEN=$(echo -n $LEN | tr --delete [:digit:])
if [ "$NLEN" != "" ]; then
    func_eprintf "passgen: Error: desired length must be entered as a base10 number."
    exit 1
fi

# we need to figure out the optimial number of 16 byte blocks to satisfy the length request.
# each 16 byte block will produce 20 characters.
NUM_BLOCKS=$((LEN / 20))
if [ $(($LEN % 20)) -gt 0 ]; then
    # partial overflow into next block
    ADDR=1
else
    # we got a number perfectly divided by 43
    ADDR=0
fi

dd if=/dev/urandom bs=16 count=$(($NUM_BLOCKS + $ADDR)) status=none | base85 | cut -c-$LEN

